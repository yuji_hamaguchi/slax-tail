(ns slax-tail.core
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [compojure.handler :as handler]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [clj-http.client :as client]
            [ring.middleware.json :as json]
            [ring.util.response :refer [response]]
            [clojure.data.json :as data-json]
            [clj-time.core :as time-core]
            [clj-time.local :as local-time]
            [clj-time.coerce :as time-coerce]
            [clj-time.format :as time-format]))

;実行例
;APIの実行
;curl -XGET "http://localhost:3002/slax/tail?user_name=foo&token=4BxgxxiihzrSU3KrSBirlTEG&team_domain=uzabase&channel_name=slax&text=tail%20-n5%20%40yuji.hamaguchi" | native2ascii -reverse -encoding UTF-8 | jq .
;JSONに対する処理
;curl -XPOST -H "Content-Type: application/json" "http://localhost:3002/slax/tail?user_name=foo&token=4BxgxxiihzrSU3KrSBirlTEG&team_domain=uzabase&channel_name=slax&text=tail%20-n2" -d @tmp.json | native2ascii -reverse -encoding UTF-8
;tmp.json
;[{"ts":"1509928291.000018",
;      "channel-name":"slax",
;  "username":"yuji.hamaguchi",
;  "text":"foobarbaz",
;  "permalink":"https:\/\/uzabase.slack.com\/archives\/C7SCETXCL\/p1509928291000018"}]

(def token "xoxp-2466069044-8547250964-262986284112-aa6312157b99ed352fe4730d443beb5f")

(defn execute-slack-api
  [n target]
  (println "called with n:" n "and target:" target ".")
  (let [result (-> (client/get "https://slack.com/api/search.messages"
                               {:query-params {"token"  token
                                               "query"  (case (first target)
                                                          \@ (str "from:" target)
                                                          \# (str "in:" target)
                                                          :else nil)
                                               "count"  n
                                               "sort"   "timestamp"
                                               "pretty" 1}}
                               {:accept :json})
                   :body
                   (data-json/read-str :key-fn keyword)
                   :messages
                   :matches)
        result' (map (fn [m] (assoc m :channel-name (:name (:channel m)))) result)]
    (data-json/write-str (map #(select-keys % [:ts :channel-name :username :text :permalink]) result'))))

(defn sx-tail
  [n ms]
  (println "called with n:" n "and" (count ms) "messages.")
  (data-json/write-str
    (reverse (take n (reverse (sort-by :ts ms))))))

(defn command-handler
  [text & sx-univ-objs]
  (let [[command opt1 target] (clojure.string/split text #"\s")
        n (Integer. (clojure.string/replace opt1 "-n" ""))
        target (if target (clojure.string/replace target #"[<>]" ""))]
    (if-not sx-univ-objs
      (execute-slack-api n target)
      (let [sx-univ-objs' (map
                            (fn [obj] (reduce (fn [acc [k v]] (assoc acc (keyword k) v)) {} obj))
                            (first sx-univ-objs))]
        (sx-tail n sx-univ-objs')))))

(defn slax-gateway
  [token team_domain channel_name text user_name & sx-univ-objs]
  (when (not (= user_name "slackbot"))
    (if (and (= token "4BxgxxiihzrSU3KrSBirlTEG")
             (= team_domain "uzabase")
             (= channel_name "slax"))
      (if-not sx-univ-objs
        (response (->> (command-handler text)))
        (response (->> (command-handler text (first sx-univ-objs)))))
      (response {:text "Authentication Failed"}))))

(defroutes app-routes
           (GET "/slax/tail" [token team_domain channel_name text user_name]
                (slax-gateway token team_domain channel_name text user_name))
           (POST "/slax/tail" {{token        :token
                                team_domain  :team_domain
                                channel_name :channel_name
                                text         :text
                                user_name    :user_name} :params
                               sx-univ-objs              :body}
                 (slax-gateway token team_domain channel_name text user_name sx-univ-objs))
           (route/not-found "Not Found"))

(def app
  (-> (handler/api app-routes)
      (json/wrap-json-body)
      (json/wrap-json-response)))